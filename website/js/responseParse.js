/*
$( document ).ready(function() {
    console.log("ResponsePage - DOM is loaded.");


    var pageURL = window.location.href;
    pageURL = pageURL.toString();


    // Gets url strings
    var paramIndex = pageURL.indexOf("#");
    if (paramIndex === -1) {
        return;
    }
    var parameters = pageURL.substring(paramIndex + 1);

    // If site was http://yoursite.com?id=abcd12345, the 'parameters'
    // variable would equal 'id=abcd12345'    
    console.log(" page url: " + pageURL);
    console.log(" url parameters: " + parameters);

    // Extracts the encoded tokens from url paramaters
    var idToken = getParameter(parameters, "id_token=");
    var accessToken = getParameter(parameters, "access_token=");
    console.log("id token: " + idToken);
    console.log("access token: " + accessToken);

    // Decodes the tokens
    var idTokenDecoded = atob(idToken.split('.')[1]);
    var accessTokenDecoded = atob(accessToken.split('.')[1]);   
    console.log("id token decoded: " + idTokenDecoded);
    console.log("access token decoded: " + accessTokenDecoded);

    // Converts the tokens to JSON
    var idTokenJson = JSON.parse(idTokenDecoded);
    var accessTokenJson = JSON.parse(accessTokenDecoded);   

    // Can now access the fields as such using JSON.parse()
    console.log("email: " +  idTokenJson.email);
    console.log("id: " +  idTokenJson.sub);
    console.log("user: " +  accessTokenJson.username);

    //document.getElementById("userName").innerHTML = "User name: " + accessTokenJson.username;
    //document.getElementById("email").innerHTML ="Email: " +  idTokenJson.email;
    document.getElementById("requestor").value = accessTokenJson.username;
    document.getElementById("requestorEmail").value = idTokenJson.email;

});

*/

function LoadUserInfo()
{
    console.log("ResponsePage - DOM is loaded.");


    var pageURL = window.location.href;
    pageURL = pageURL.toString();


    // Gets url strings
    var paramIndex = pageURL.indexOf("#");
    if (paramIndex === -1) {
        return;
    }
    var parameters = pageURL.substring(paramIndex + 1);

    // If site was http://yoursite.com?id=abcd12345, the 'parameters'
    // variable would equal 'id=abcd12345'    
    console.log(" page url: " + pageURL);
    console.log(" url parameters: " + parameters);

    // Extracts the encoded tokens from url paramaters
    var idToken = getParameter(parameters, "id_token=");
    var accessToken = getParameter(parameters, "access_token=");
    console.log("id token: " + idToken);
    console.log("access token: " + accessToken);

    // Decodes the tokens
    var idTokenDecoded = atob(idToken.split('.')[1]);
    var accessTokenDecoded = atob(accessToken.split('.')[1]);   
    console.log("id token decoded: " + idTokenDecoded);
    console.log("access token decoded: " + accessTokenDecoded);

    // Converts the tokens to JSON
    var idTokenJson = JSON.parse(idTokenDecoded);
    var accessTokenJson = JSON.parse(accessTokenDecoded);   

    // Can now access the fields as such using JSON.parse()
    console.log("email: " +  idTokenJson.email);
    console.log("id: " +  idTokenJson.sub);
    console.log("user: " +  accessTokenJson.username);

    //document.getElementById("userName").innerHTML = "User name: " + accessTokenJson.username;
    //document.getElementById("email").innerHTML ="Email: " +  idTokenJson.email;
    //document.getElementById("requestor").value = accessTokenJson.username;
    //document.getElementById("requestorEmail").value = idTokenJson.email;
    userName =  accessTokenJson.username;
    userEmail = idTokenJson.email;
}
/**
 * Takes the url parameters and extracts the field that matches the 'param'
 * input.
 * @param {type} url, contains the URL paramaters
 * @param {type} param, field to look for in url
 * @returns {unresolved} the param value;
 */

function getParameter(url, param) {
    var urlVars = url.split('&');
    var returnValue;
    for (var i = 0; i < urlVars.length; i++) {
        var urlParam = urlVars[i];

        // get up to index
        var index = urlParam.toString().indexOf('=');
        urlParam = urlParam.substring(0, index + 1);
        if (param === urlParam) {
            returnValue = urlVars[i].replace(param, '');
            i = urlVars.length; // exits the loop
        }
    }
    return returnValue;
}