    var INVAID_DATA = "Invalid data";
    function UploadProcess() {
        requestPersons = "";
        requestBirthdays = "";
        requestZodiacSigns = "";
        totalRows = 0;
        totalInvalidRows = 0;
        //Reference the FileUpload element.
        var fileUpload = document.getElementById("fileUpload");
 
        //Validate whether File is valid Excel file.
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
        if (regex.test(fileUpload.value.toLowerCase())) {
            if (typeof (FileReader) != "undefined") {
                var reader = new FileReader();
 
                //For Browsers other than IE.
                if (reader.readAsBinaryString) {
                    reader.onload = function (e) {
                        GetTableFromExcel(e.target.result);
                    };
                    reader.readAsBinaryString(fileUpload.files[0]);
                } else {
                    //For IE Browser.
                    reader.onload = function (e) {
                        var data = "";
                        var bytes = new Uint8Array(e.target.result);
                        for (var i = 0; i < bytes.byteLength; i++) {
                            data += String.fromCharCode(bytes[i]);
                        }
                        GetTableFromExcel(data);
                    };
                    reader.readAsArrayBuffer(fileUpload.files[0]);
                }
            } else {
                alert("This browser does not support HTML5.");
            }
        } else {
            alert("Please upload a valid Excel file.");
        }
    };
    function GetTableFromExcel(data) {
        //Read the Excel File data in binary
        var workbook = XLSX.read(data, {
            type: 'binary'
        });
 
        //get the name of First Sheet.
        var Sheet = workbook.SheetNames[0];
 
        //Read all rows from First Sheet into an JSON array.
        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[Sheet]);
        BuildDisplayTable(excelRows);      

    };

    function BuildDisplayTable(excelRows)
    {
                //Create a HTML Table element.
                var myTable  = document.createElement("table");
                myTable.border = "1";
                myTable.setAttribute("id","requests");
         
                //Add the header row.
                var row = myTable.insertRow(-1);
         
                //Add the header cells.
                var headerCell = document.createElement("TH");
                headerCell.innerHTML = "#";
                row.appendChild(headerCell);
        
                headerCell = document.createElement("TH");
                headerCell.innerHTML = "Person";
                row.appendChild(headerCell);
         
                headerCell = document.createElement("TH");
                headerCell.innerHTML = "Birthday";
                row.appendChild(headerCell);
        
                headerCell = document.createElement("TH");
                headerCell.innerHTML = "Zodiac Sign";
                row.appendChild(headerCell);
        
                headerCell = document.createElement("TH");
                headerCell.innerHTML = "Alternative Name";
                row.appendChild(headerCell);
        
                headerCell = document.createElement("TH");
                headerCell.innerHTML = "Character";
                row.appendChild(headerCell);
        
                headerCell = document.createElement("TH");
                headerCell.innerHTML = "Zodiac Icon";
                row.appendChild(headerCell);
        
                headerCell = document.createElement("TH");
                headerCell.innerHTML = "Zodiac Thumnail";
                row.appendChild(headerCell);
        
                requestDataCSV = "Person,Birthday,Sign\n";
                //Add the data rows from Excel file.
                totalRows = excelRows.length;
                for (var i = 0; i < excelRows.length; i++) {
                    //Add the data row.
                    var row = myTable.insertRow(-1);
         
                    //Add the data cells.
                    var cell = row.insertCell(-1);
                    cell.innerHTML = i+1;
        
                    cell = row.insertCell(-1);
                    cell.innerHTML = excelRows[i].Person; 
        
                    var birthDate = "2022"+excelRows[i].Birthday;
                    console.log("Birthdate - "+i+" = "+birthDate);
                    const d = new Date(birthDate);
                    var sign = "";
                    if(d.getFullYear()<0 && d.getFullYear()>2023)
                        sign = INVAID_DATA;
                    else
                        sign = findZodiacSign(d.getDate(),d.getMonth());
                    
                    cell = row.insertCell(-1);
                    if(sign==INVAID_DATA)
                        cell.innerHTML = "<font color='red'>"+excelRows[i].Birthday+"</font>";
                    else
                        cell.innerHTML = excelRows[i].Birthday;    
        
                    cell = row.insertCell(-1);
                    if(sign==INVAID_DATA)
                        cell.innerHTML = "<font color='red'>"+INVAID_DATA+"</font>";
                    else
                        cell.innerHTML = sign;
                    
                    cell = row.insertCell(-1);
                    if(sign==INVAID_DATA)
                        cell.innerHTML = "";
                    else
                        cell.innerHTML = findZodiacAltName(sign);
        
                    cell = row.insertCell(-1);
                    if(sign==INVAID_DATA)
                        cell.innerHTML = "";
                    else
                        cell.innerHTML = findZodiacHexCodes(sign);
        
                    cell = row.insertCell(-1);
        
                    if(sign==INVAID_DATA)
                    {
                        cell.innerHTML = "";
                        totalInvalidRows++;
                    }
                    else
                        cell.innerHTML = "<img src=\"./images/"+sign+"_icon.png\" alt=\""+sign+"\" style=\"width:30px\">";
        
                    cell = row.insertCell(-1);
                    if(sign==INVAID_DATA)
                        cell.innerHTML = "";
                    else
                        cell.innerHTML = "<img src=\"./images/"+sign+".png\" alt=\""+sign+"\" style=\"width:100px\">";
        
                    if(sign!=INVAID_DATA)
                    {
                        requestPersons += excelRows[i].Person;
                        requestBirthdays += excelRows[i].Birthday;
                        requestZodiacSigns += sign;
                    }
                    if(i < excelRows.length-1)
                    {
                        requestPersons += ",";
                        requestBirthdays += ",";
                        requestZodiacSigns += ",";
                    }
                    requestDataCSV += excelRows[i].Person+","+excelRows[i].Birthday+","+sign+"\n";
                   
                }        
         
                var ExcelTable = document.getElementById("requestTable");
                ExcelTable.innerHTML = "";
                ExcelTable.appendChild(myTable);
                var totalRowsDiv = document.getElementById("totalRows");
                var tableMsg = "";
                if(totalRows==0)
                    tableMsg += "Total rows - 0. <font color='red'>Empty request file</font>";
                else if(totalRows>MAX_ROWS_ALLOWED)
                    tableMsg += "Total rows - "+totalRows+", <font color='red'>Request file exceeds maximum allowed ("+MAX_ROWS_ALLOWED+")</font>";
                else 
                    tableMsg += "Total rows - "+totalRows;
                if(totalInvalidRows>0)
                    tableMsg += " , <font color='red'>Invalid rows - "+totalInvalidRows+"</font>";
        
                totalRowsDiv.innerHTML = tableMsg;
                var csvTextArea = document.getElementById("requestDataTextArea");
                if(csvTextArea!=null)
                csvTextArea.value = requestDataCSV;
        
                var requestData = document.getElementById("requestData");
                if(requestData!=null)
                requestData.value = requestPersons+"\n"+requestBirthdays+"\n"+requestZodiacSigns;
    }
