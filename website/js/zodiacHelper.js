var zodiacAltNames = {
    'Capricorn':'The Goat',
    'Aquarius':'The Water-bearer',
    'Pisces':'The Fish',
    'Aries':'The Ram',
    'Taurus':'The Bull',
    'Gemini':'The Twins',
    'Cancer':'The Crab',
    'Leo':'The Lion',
    'Virgo':'The Maiden',
    'Libra':'The Scales',
    'Scorpio':'The Scorpion',
    'Sagittarius':'The Archer'
  }
  var zodiaHexCodes = {
    'Capricorn':'&#9809',
    'Aquarius':'&#9810;',
    'Pisces':'&#9811;',
    'Aries':'&#9800',
    'Taurus':'&#9801',
    'Gemini':'&#9802',
    'Cancer':'&#9803',
    'Leo':'&#9804',
    'Virgo':'&#9805',
    'Libra':'&#9806',
    'Scorpio':'&#9807',
    'Sagittarius':'&#9808'
  }
function findZodiacAltName(sign)
{
    return zodiacAltNames[sign];
}
function findZodiacHexCodes(sign)
{
    return zodiaHexCodes[sign];
}

function findZodiacSign(day, month) {

    var zodiacSigns = {
      'capricorn':'Capricorn',
      'aquarius':'Aquarius',
      'pisces':'Pisces',
      'aries':'Aries',
      'taurus':'Taurus',
      'gemini':'Gemini',
      'cancer':'Cancer',
      'leo':'Leo',
      'virgo':'Virgo',
      'libra':'Libra',
      'scorpio':'Scorpio',
      'sagittarius':'Sagittarius'
    }

  
    if((month == 0 && day <= 19) || (month == 11 && day >=22)) {
      return zodiacSigns.capricorn;
    } else if ((month == 0 && day >= 20) || (month == 1 && day <= 18)) {
      return zodiacSigns.aquarius;
    } else if((month == 1 && day >= 19) || (month == 2 && day <= 20)) {
      return zodiacSigns.pisces;
    } else if((month == 2 && day >= 21) || (month == 3 && day <= 19)) {
      return zodiacSigns.aries;
    } else if((month == 3 && day >= 20) || (month == 4 && day <= 20)) {
      return zodiacSigns.taurus;
    } else if((month == 4 && day >= 21) || (month == 5 && day <= 20)) {
      return zodiacSigns.gemini;
    } else if((month == 5 && day >= 21) || (month == 6 && day <= 22)) {
      return zodiacSigns.cancer;
    } else if((month == 6 && day >= 23) || (month == 7 && day <= 22)) {
      return zodiacSigns.leo;
    } else if((month == 7 && day >= 23) || (month == 8 && day <= 22)) {
      return zodiacSigns.virgo;
    } else if((month == 8 && day >= 23) || (month == 9 && day <= 22)) {
      return zodiacSigns.libra;
    } else if((month == 9 && day >= 23) || (month == 10 && day <= 21)) {
      return zodiacSigns.scorpio;
    } else if((month == 10 && day >= 22) || (month == 11 && day <= 21)) {
      return zodiacSigns.sagittarius;
    }
    return "Invalid data";
  }