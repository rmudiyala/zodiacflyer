<script>
 // setTimeout(() => {  console.log("World!"); }, 5000);
  //  function showGrid(){
        const columnDefs = [
  { field: "id" },{ field: "Requestor" },{ field: "Contacts" },{ field: "Persons" },{ field: "BirthDays" },{ field: "ZodiacSigns" },{ field: "RequestStatus" },{ field: "ProcessedDate" }
  ];
  
  // specify the data
  const rowData = [
  { id: "1", Requestor: "Ravi", Contacts: "Charles,Jonathan", Persons: "Martin Luther King Jr,Vince Lambardi", BirthDays: "16-Jan,11-Jun", ZodiacSigns: "Capricorn,Gemini", RequestStatus: "Completed", ProcessedDate: "10/30/2022 3:40 PM" },
  { id: "2", Requestor: "Charles", Contacts: "Daniel,Cierra", Persons: "Frank Sinatra", BirthDays: "12-Dec", ZodiacSigns: "Sagittarius", RequestStatus: "In Process", ProcessedDate: "" },
  { id: "3", Requestor: "Jonathan", Contacts: "Cierra,Ravi", Persons: "Mother Theresa", BirthDays: "26-Aug", ZodiacSigns: "Virgo", RequestStatus: "Completed", ProcessedDate: "10/29/2022 1:21 PM" }

  ];

  //alert("rowData = "+rowData[1].Requestor);
  //const rowData2 = json_output_obj;
  //("rowData2 = "+rowData2[1].Requestor);
  
  // let the grid know which columns and what data to use
  const gridOptions = {
    defaultColDef: {
    resizable: true,
    sortable: true,
    filter: true
  },
  pagination: true,
  columnDefs: columnDefs,
  rowData: rowData//json_output_obj
  };
  //alert("Showing grid");
  // setup the grid after the page has finished loading
  document.addEventListener('DOMContentLoaded', () => {
    const gridDiv = document.querySelector('#myGrid');
    new agGrid.Grid(gridDiv, gridOptions);
  });
  //gridOptions.api.sizeColumnsToFit();
  //gridOptions.api.setRowData(json_output_obj);
  //alert("Aftering showing grid");
  //  }
  
  </script>