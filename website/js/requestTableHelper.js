function BuildDisplayTable(persons,birthdays,signs, flyerURLs,requestStatus)
{
            var personList = persons.split(',');
            var birthdaysList = birthdays.split(',');
            var signsList = signs.split(',');
            var flyerURLList = flyerURLs.split(',');

            //Create a HTML Table element.
            var myTable  = document.createElement("table");
            myTable.border = "1";
            myTable.setAttribute("id","requests");
     
            //Add the header row.
            var row = myTable.insertRow(-1);
     
            //Add the header cells.
            var headerCell = document.createElement("TH");
            headerCell.innerHTML = "#";
            row.appendChild(headerCell);
    
            headerCell = document.createElement("TH");
            headerCell.innerHTML = "Person";
            row.appendChild(headerCell);
     
            headerCell = document.createElement("TH");
            headerCell.innerHTML = "Birthday";
            row.appendChild(headerCell);
    
            headerCell = document.createElement("TH");
            headerCell.innerHTML = "Zodiac Sign";
            row.appendChild(headerCell);
    
            headerCell = document.createElement("TH");
            headerCell.innerHTML = "Flyer Thumbnail";
            row.appendChild(headerCell);

            headerCell = document.createElement("TH");
            headerCell.innerHTML = "Flyer Link";
            row.appendChild(headerCell);
    
            requestDataCSV = "Person,Birthday,Sign\n";
            //Add the data rows from Excel file.
            totalRows = personList.length;
            for (var i = 0; i < personList.length; i++) {
                //Add the data row.
                var row = myTable.insertRow(-1);
     
                //Add the data cells.
                var cell = row.insertCell(-1);
                cell.innerHTML = i+1;
    
                cell = row.insertCell(-1);
                cell.innerHTML = personList[i]; 
                  
                cell = row.insertCell(-1);
                cell.innerHTML = birthdaysList[i];    
    
                cell = row.insertCell(-1);
                cell.innerHTML = signsList[i];
      
                cell = row.insertCell(-1);
                if(requestStatus=="Processed")
                    cell.innerHTML = "<img src=\""+flyerURLList[i]+"\" style=\"width:150px;height:150px\">";     
                else
                cell.innerHTML = "";
                
                cell = row.insertCell(-1);
                if(requestStatus=="Processed")
                    cell.innerHTML = "<a href=\""+flyerURLList[i]+"\">"+personList[i]+"_"+signsList[i]+".png</a>";
                else
                    cell.innerHTML = "";
            }        
     
            var ExcelTable = document.getElementById("requestTable");
            ExcelTable.innerHTML = "";
            ExcelTable.appendChild(myTable);

            var totalRowsDiv = document.getElementById("totalRows");
            var tableMsg = "Total rows - "+totalRows;    
            totalRowsDiv.innerHTML = tableMsg;
    
}
