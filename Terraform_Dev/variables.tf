variable "account_id" {
  type    = string
  default = "918967593922" # Danielle
  #default = "577015061725"   # Ravi
  #default = "877111532311"   # Charles
}

variable "region" {
  type    = string
  default = "us-east-1"
}
variable "account" {
  type    = string
  default = "Sparx/Innovation Launchpad"
}

variable "environment" {
  type    = string
  default = "uat"
}

variable "cognitodomain" {
  type    = string
  default = "zodiacflyeruat"
}
variable "team" {
  type    = string
  default = "Team 3"
}

variable "appversion" {
  type    = string
  default = "Final Demo - 11/17"
}

variable "description_suffix" {
  type    = string
  default = " - Provisioned using Terraform/IaC"
}
variable "team_members" {
  type    = string
  default = "Cierra Charles Dan Jonathan Ravi"
}

variable "application" {
  type    = string
  default = "ZodiacFlyer"
}

variable "backendbucket" {
  type    = string
  default = "zodiacflyer-team3-backend"
}

variable "inputbucket" {
  type    = string
  default = "zodiacflyer-team3-input"
}

variable "logbucket" {
  type    = string
  default = "zodiacflyer-team3-logging"
}

variable "websitebucket" {
  type    = string
  default = "zodiacflyer-team3-website"
}

variable "outputbucket" {
  type    = string
  default = "zodiacflyer-team3-output"
}



variable "dynamodbtable" {
  type    = string
  default = "ZodiacFlyer_Requests"
}

variable "datapath" {
  type    = string
  default = "D:/Users/ravindra.mudiyala/REPO/ZodiacFlyer/Terraform_Dev/data"
}

variable "sender_email" {
  type    = string
  default = "awsccteam3@gmail.com"
}

variable "function1" {
  type    = string
  default = "GenerateFlyer"
}

variable "function2" {
  type    = string
  default = "StoreRequest"
}

variable "function3" {
  type    = string
  default = "SendEmail"
}

variable "function4" {
  type    = string
  default = "PerformCRUDOps"
}


