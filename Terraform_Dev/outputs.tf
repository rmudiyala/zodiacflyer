# Output value definitions

output "input_bucket" {
  description = "Name of the input bucket"
  value       = aws_s3_bucket.input-bucket.id
}

output "output_bucket" {
  description = "Name of the output bucket"
  value       = aws_s3_bucket.output-bucket.id
}

output "website_bucket" {
  description = "Name of the website bucket"
  value       = aws_s3_bucket.website-bucket.id
}

output "log_bucket" {
  description = "Name of the logging bucket"
  value       = aws_s3_bucket.log-bucket.id
}

output "Lambda_Function1_name" {
  description = "Name of the Lambda function."
  value       = aws_lambda_function.Lambda_Function1.function_name
}

output "Lambda_Function2_name" {
  description = "Name of the Lambda function."
  value       = aws_lambda_function.Lambda_Function2.function_name
}

output "Lambda_Function3_name" {
  description = "Name of the Lambda function."
  value       = aws_lambda_function.Lambda_Function3.function_name
}

output "Lambda_Function4_name" {
  description = "Name of the Lambda function."
  value       = aws_lambda_function.Lambda_Function4.function_name
}

output "website_url" {
  description = "Website url"
  value       = "https://${aws_s3_bucket.website-bucket.id}.s3.amazonaws.com"
}

output "apigateway_url" {
  description = "API Gateway Endpoint"
  value       = "https://${aws_apigatewayv2_api.apigw_crud.id}.execute-api.${var.region}.amazonaws.com/stage1"
}

output "cloudfront_url" {
  description = "CloudFront URL for S3-based website"
  value       = "https://${aws_cloudfront_distribution.s3_distribution.domain_name}"
}

output "cognito_app_client" {
  description = "Cognito app client id"
  value       = aws_cognito_user_pool_client.userpool_client.id
}

output "cognito_hostedui" {
  description = "Cognito hosted ui"
  value       = "https://${aws_cognito_user_pool_domain.userpool_domain.domain}.auth.us-east-1.amazoncognito.com"
}

output "waf_id" {
  description = "WAF Id"
  value       = aws_wafv2_web_acl.waf1.id
}
