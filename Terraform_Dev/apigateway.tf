##########################    ZodiacFlyer - API Gateway using HTTP ######################
# Implements all of the CRUD operations required to store/retrieve/update flyer request #

resource "aws_apigatewayv2_api" "apigw_crud" {
  name          = "${var.application}-${var.environment}"
  protocol_type = "HTTP"

  cors_configuration {
    allow_origins = ["*"]
    allow_methods = ["GET", "POST", "PUT", "DELETE", "HEAD"]
    allow_headers = ["*"]
    max_age       = 300
  }
}

resource "aws_apigatewayv2_stage" "apigw_crud_stage1" {
  api_id = aws_apigatewayv2_api.apigw_crud.id

  name        = "stage1"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_gw.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
}

resource "aws_apigatewayv2_integration" "integration1" {
  api_id = aws_apigatewayv2_api.apigw_crud.id

  integration_uri    = aws_lambda_function.Lambda_Function4.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_apigatewayv2_route" "route1" {
  api_id = aws_apigatewayv2_api.apigw_crud.id

  route_key = "GET /items"
  target    = "integrations/${aws_apigatewayv2_integration.integration1.id}"
}

resource "aws_apigatewayv2_integration" "integration2" {
  api_id = aws_apigatewayv2_api.apigw_crud.id

  integration_uri    = aws_lambda_function.Lambda_Function2.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_apigatewayv2_route" "route2" {
  api_id = aws_apigatewayv2_api.apigw_crud.id

  route_key = "POST /itemadd"
  target    = "integrations/${aws_apigatewayv2_integration.integration2.id}"
}

resource "aws_apigatewayv2_route" "route3" {
  api_id = aws_apigatewayv2_api.apigw_crud.id

  route_key = "GET /items/{id}"
  target    = "integrations/${aws_apigatewayv2_integration.integration1.id}"
}

resource "aws_apigatewayv2_route" "route4" {
  api_id = aws_apigatewayv2_api.apigw_crud.id

  route_key = "DELETE /items/{id}"
  target    = "integrations/${aws_apigatewayv2_integration.integration1.id}"
}

resource "aws_apigatewayv2_route" "route5" {
  api_id = aws_apigatewayv2_api.apigw_crud.id

  route_key = "PUT /items/{id}"
  target    = "integrations/${aws_apigatewayv2_integration.integration1.id}"
}

resource "aws_cloudwatch_log_group" "api_gw" {
  name              = "/aws/api_gw/${aws_apigatewayv2_api.apigw_crud.name}"
  retention_in_days = 7
  #kms_key_id = aws_kms_key.apigw_lambda_key.arn                            
}

resource "aws_lambda_permission" "api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.Lambda_Function4.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.apigw_crud.execution_arn}/*/*"
}
