terraform {
  backend "s3" {
    bucket = "zodiacflyer-team3-backend-uat"
    key    = "s3-terraform-uat.tfstate"
    acl    = "bucket-owner-full-control"
    region = "us-east-1"
  }
}