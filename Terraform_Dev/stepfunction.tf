resource "aws_sfn_state_machine" "sfn_state_machine" {
  name     = "${var.application}-StepFunction-${var.environment}"
  type     = "EXPRESS"
  role_arn = aws_iam_role.StepFunction_Role.arn

  definition = <<EOF
{
  "Comment": "${var.application} StepFunction to generate flyers asynchronously",
  "StartAt": "Wait",
  "States": {
    "Wait": {
      "Type": "Wait",
      "SecondsPath": "$.waitSeconds",
      "Next": "${var.function1}"
    },
    "${var.function1}": {
      "Type": "Task",
      "Resource": "arn:aws:lambda:${var.region}:${var.account_id}:function:${aws_lambda_function.Lambda_Function1.function_name}:$LATEST",
      "ResultPath": null,
      "End": true
    }
  }
}
EOF
}

resource "aws_iam_role" "StepFunction_Role" {
  name = "${var.application}-${var.environment}-StepFunction-Role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "stepfunction_policy" {
  role       = aws_iam_role.StepFunction_Role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

