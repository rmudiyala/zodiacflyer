resource "aws_dynamodb_table" "zodiacflyer-requests" {
  name         = "${var.dynamodbtable}_${var.environment}"
  billing_mode = "PAY_PER_REQUEST"
  #read_capacity  = 20
  #write_capacity = 20
  hash_key  = "id"
  range_key = "Requestor"

  attribute {
    name = "id"
    type = "S"
  }

  attribute {
    name = "Requestor"
    type = "S"
  }

  server_side_encryption {
    enabled     = true
    kms_key_arn = aws_kms_key.dynamodb_key.arn
  }

  point_in_time_recovery {
    enabled = true
  }

  /*
  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }
*/
}

/*
resource "aws_dynamodb_table_item" "zodiacflyer-requests-item" {
  table_name = aws_dynamodb_table.zodiacflyer-requests.name
  hash_key   = aws_dynamodb_table.zodiacflyer-requests.hash_key

  item = <<ITEM
 {"id":{"S": "2"}, 
 "Requestor": { "S": "Ravi" },
 "Contacts": { "S": "Jonathan" }, 
 "Persons": { "S": "Frank Sinatra" },  
 "Birthdays": { "S": "12-Dec" }, 
 "ZodiacSigns": { "S": "PiscSagittarius" }, 
 "FlyerStatus": {"S": "" } , 
 "RequestStatus": { "S": "New" }, 
 "FlyerURLs": { "S": "http://url2" },  
 "RequestDate": { "S": "" },  
 "ProcessedDate": { "S": "" }
 }
ITEM
}
*/

/*
resource "aws_dynamodb_table_item" "zodiacflyer-requests-items" {
  for_each = local.tf_data
  table_name = aws_dynamodb_table.zodiacflyer-requests.name
  hash_key   = "id"
  item = jsonencode(each.value)
}
*/