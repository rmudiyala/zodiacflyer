resource "aws_kms_key" "log_key" {
  #name = "${var.application}-logkey-${var.environment}"
  enable_key_rotation = true
}

resource "aws_kms_key" "dynamodb_key" {
  #name = "${var.application}-dynamodbkey-${var.environment}"
  enable_key_rotation = true
}

resource "aws_kms_key" "s3_key" {
  #name = "${var.application}-s3key-${var.environment}"
  enable_key_rotation = true
}

resource "aws_kms_key" "apigw_lambda_key" {
  #name = "${var.application}-apigwlambda-${var.environment}"
  enable_key_rotation = true
}