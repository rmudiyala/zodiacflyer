const AWS = require("aws-sdk");

const dynamo = new AWS.DynamoDB.DocumentClient();

exports.lambda_handler = async (event, context) => {
  let body;
  let statusCode = 200;
  let appTableName = "ZodiacFlyer_Requests";
  
  const headers = {
    "Content-Type": "application/json"
  };

  try {
    switch (event.routeKey) {
      case "DELETE /items/{id}":
        await dynamo
          .delete({
            TableName: appTableName,
            Key: {
              id: event.pathParameters.id
            }
          })
          .promise();
        body = `Deleted item ${event.pathParameters.id}`;
        break;
      case "GET /items/{id}":
        body = await dynamo
          .get({
            TableName: appTableName,
            Key: {
              id: event.pathParameters.id
            }
          })
          .promise();
        break;
      case "GET /items":
        body = await dynamo
          .scan({ TableName: appTableName })
          .promise();
        break;
      case "GET /itemsrequestor/{requestor}":
          let params = {
        TableName: appTableName,
        FilterExpression: "Requestor = :requestor",
        ExpressionAttributeValues: {
            ":requestor"    :   event.pathParameters.requestor
        }
        };
        body = await dynamo
          .scan({ TableName: appTableName })
          .promise();
        break;
      case "PUT /items":
        let requestJSON = JSON.parse(event.body);
        await dynamo
          .put({
            TableName: appTableName,
            Item: {
              id: requestJSON.id,
              Requestor: requestJSON.Requestor,
              RequestorEmail: requestJSON.RequestorEmail,
              RequestCount:requestJSON.RequestCount,
              Contacts: requestJSON.Contacts,
              Persons: requestJSON.Persons,
              Birthdays: requestJSON.Birthdays,
              ZodiacSigns: requestJSON.ZodiacSigns,
              FlyerStatus: requestJSON.FlyerStatus,
              RequestStatus: requestJSON.RequestStatus,
              FlyerURLs: requestJSON.RequestStatus,
              RequestDate: requestJSON.RequestDate,
              ProcessedDate: requestJSON.ProcessedDate

            }
          })
          .promise();
        body = `Put item ${requestJSON.id}`;
        break;
        case "POST /items":
        let requestJSON2 = JSON.parse(event.body);
        await dynamo
          .put({
            TableName: appTableName,
            Item: {
              id: requestJSON2.id,
              Requestor: requestJSON2.Requestor,
              RequestorEmail: requestJSON2.RequestorEmail,
              RequestCount:requestJSON2.RequestCount,
              Contacts: requestJSON2.Contacts,
              Persons: requestJSON2.Persons,
              Birthdays: requestJSON2.Birthdays,
              ZodiacSigns: requestJSON2.ZodiacSigns,
              FlyerStatus: requestJSON2.FlyerStatus,
              RequestStatus: requestJSON2.RequestStatus,
              FlyerURLs: requestJSON2.RequestStatus,
              RequestDate: requestJSON.RequestDate,
              ProcessedDate: requestJSON.ProcessedDate
            }
          })
          .promise();
        body = `Post item ${requestJSON2.id}`;
        break;
      default:
        throw new Error(`Unsupported route: "${event.routeKey}"`);
    }
  } catch (err) {
    statusCode = 400;
    body = err.message;
  } finally {
    body = JSON.stringify(body);
  }

  return {
    statusCode,
    body,
    headers
  };
};
