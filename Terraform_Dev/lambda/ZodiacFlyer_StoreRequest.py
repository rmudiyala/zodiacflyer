import json
import os
import uuid
import boto3

from datetime import datetime
from botocore.exceptions import ClientError

CHARSET = 'UTF-8'
SFN_ARN =  os.environ['STEPFUNCTION_ARN']
DYNAMODB_TABLE = os.environ['DYNAMODB_TABLE']
SENDER_EMAIL = os.environ['SENDER_EMAIL']  # Must be configured in SES
SES_REGION = os.environ['SES_REGION']

dynamodb = boto3.resource('dynamodb')
ses = boto3.client('ses', region_name=SES_REGION)
sfn = boto3.client('stepfunctions')

def lambda_handler(event, context):
    print("====================================== Context ========================================================")
    print(context)
    print("====================================== Event ========================================================")
    print(event)
    data = json.loads(event['body'])

    try:

        separator = '<br/>'
        contentLine1 = 'New request '+data['id']+' received.'+separator
        contentLine2 = 'Requestor - '+data['Requestor']+separator
        contentLine3 = 'Requestor Email - '+data['RequestorEmail']+separator
        contentLine4 = 'Contacts - '+data['Contacts']+separator
        contentLine5 = 'Persons - '+data['Persons']+separator
        contentLine6 = 'Birthdays - '+data['Birthdays']+separator
        contentLine7 = 'Zodiac Signs - '+data['ZodiacSigns']+separator
        contentLine8 = 'Request Date - '+data['RequestDate']+separator
        contentLine9 = 'Request Count - '+data['RequestCount']+separator
        
        content = contentLine1+contentLine2+contentLine3+contentLine4+contentLine5+contentLine6+contentLine7+contentLine8+contentLine9
        print(content)
        print("Before saving to DynamoDB")
        save_to_dynamodb(data)
        print("After saving to DynamoDB, before sending email")
        response = send_mail_to_user(data, content,data['RequestorEmail'])
        contacts = data['Contacts']
        print("Contacts = '"+contacts+"'")
        if(len(contacts)>0):
             send_mail_to_user(data, content,contacts)
        print("Before starting stepfunction")
        data['waitSeconds'] = 1
        response = sfn.start_execution(stateMachineArn=SFN_ARN,input=json.dumps(data, cls=DecimalEncoder))
        print("After starting stepfunction")

        
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Before return statuscode")
    
    return {
        "statusCode": 200,
        "headers": {"Content-Type": "application/json"},
        "body": ""
    }


def save_to_dynamodb(data):
    

    table = dynamodb.Table(DYNAMODB_TABLE)
    item = {
        'id':data['id'],
        'Requestor': data['Requestor'],  # required
        'RequestorEmail': data['RequestorEmail'],  # required,
        'RequestCount': data['RequestCount'],
        'Contacts': data['Contacts'] if data['Contacts'] else None,
        'Persons': data['Persons'],
        'Birthdays': data['Birthdays'],
        'ZodiacSigns': data['ZodiacSigns'],
        'RequestDate': data['RequestDate'],
        'FlyerStatus': data['FlyerStatus'],
        'RequestStatus': data['RequestStatus'],
        'FlyerURLs': data['FlyerURLs']
    }
    table.put_item(Item=item)
    return


def send_mail_to_user(data, content,emailIds):
    return ses.send_email(
        Source=SENDER_EMAIL,
        Destination={
            'ToAddresses': [
                emailIds,
            ]
        },
        Message={
            'Subject': {
                'Charset': CHARSET,
                'Data': 'ZodiacFlyer - New Request Received - ' + data['id']
            },
            'Body': {
                'Html': {
                    'Charset': CHARSET,
                    'Data': content
                },
                'Text': {
                    'Charset': CHARSET,
                    'Data': content
                }
            }
        }
    )
    
# This is a workaround for: http://bugs.python.org/issue16535
class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, decimal.Decimal):
            return int(obj)
        return super(DecimalEncoder, self).default(obj)
        

