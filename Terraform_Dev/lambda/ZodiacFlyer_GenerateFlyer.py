import os
import tempfile
import boto3
import json
import datetime

from PIL import Image, ImageDraw, ImageFont

#from zipfile import ZipFile

s3 = boto3.client('s3')
ses = boto3.client('ses')

DYNAMODB_TABLE = os.environ['DYNAMODB_TABLE']
SOURCE_BUCKET = os.environ['SOURCE_BUCKET']
DEST_BUCKET = os.environ['DEST_BUCKET']
SENDER_EMAIL = os.environ['SENDER_EMAIL']
SES_REGION = os.environ['SES_REGION']
S3_WEBSITE_URL = os.environ['S3_WEBSITE_URL']
CHARSET = 'UTF-8'

SIZE = 128, 128
CHARSET = "UTF-8"

class EST5EDT(datetime.tzinfo):

    def utcoffset(self, dt):
        return datetime.timedelta(hours=-5) + self.dst(dt)

    def dst(self, dt):
        d = datetime.datetime(dt.year, 3, 8)        #2nd Sunday in March
        self.dston = d + datetime.timedelta(days=6-d.weekday())
        d = datetime.datetime(dt.year, 11, 1)       #1st Sunday in Nov
        self.dstoff = d + datetime.timedelta(days=6-d.weekday())
        if self.dston <= dt.replace(tzinfo=None) < self.dstoff:
            return datetime.timedelta(hours=1)
        else:
            return datetime.timedelta(0)

    def tzname(self, dt):
        return 'EST5EDT'

dynamodb = boto3.resource('dynamodb')

#def list_buckets():
#    response = s3.list_buckets() 
    # Output the bucket names
#    print('Existing buckets:')
#    for bucket in response['Buckets']: # For Loop to list all the buckets
#        print(f'{bucket["Name"]}')

def upload_thumbnails(requestId,persons,birthdays,signs):
    s3.put_object(Bucket=DEST_BUCKET, Key=(requestId+'/'))
    print("created folder - "+requestId)
    personList = persons.split(",")
    birthdayList = birthdays.split(",")
    signList = signs.split(",")
    print("Parsed list")
    preSignedURLs = "";

    for i in range(len(personList)):
        print("Calling upload_thumbnail for - ")
        preSignedURLs += upload_thumbnail(requestId,personList[i],birthdayList[i],signList[i])+","
    return preSignedURLs[:-1]
  
def upload_thumbnail(requestId,person,birthday,sign):
    print("upload_thumbnail - "+person+","+birthday+","+sign)
    preSignedURL = ""
    source_bucket = SOURCE_BUCKET
    key = sign+".png"
    thumb = person.replace(",","").replace(" ","") +"_"+ key
    print("Thumb - "+thumb)
    msg = person+" "+birthday+" "+sign
    with tempfile.TemporaryDirectory() as tmpdir:
            download_path = os.path.join(tmpdir, key)
            upload_path = os.path.join(tmpdir, thumb)
            s3.download_file(source_bucket, key, download_path)
            #generate_thumbnail(download_path, upload_path,person)
            generate_flyer(download_path, upload_path,msg)
            s3.upload_file(upload_path, DEST_BUCKET, requestId+"/"+thumb)
            preSignedURL = boto3.client('s3').generate_presigned_url(ClientMethod='get_object',Params={'Bucket': DEST_BUCKET, 'Key': requestId+"/"+thumb},ExpiresIn=84000)

    print('Thumbnail image saved at {}/{}'.format(DEST_BUCKET, requestId+"/"+thumb))
    print("PresignedURL - "+preSignedURL)
    return preSignedURL
    

def generate_thumbnail(source_path, dest_path,person):
    print('Generating thumbnail from:', source_path)
    with Image.open(source_path) as image:
        image.thumbnail(SIZE)
        image.save(dest_path)

def generate_flyer(source_path, dest_path,msg):
    print('Generating flyer from:', source_path)
    with Image.open(source_path) as image:
        d1 = ImageDraw.Draw(image)
       # myFont = ImageFont.truetype('Game of Thrones.ttf', 40)
       # myfont = ImageFont.truetype("arial.ttf", 36)
       # d1.text((200, 200), person,font=myfont, fill =(255, 0, 0))
        d1.text((200, 200), msg, fill =(255, 0, 0))
        print("Completed drawing")
        image.save(dest_path)

def update_to_dynamodb(data,requestStatus,flyerStatus,preSignedURLs):
    x = datetime.datetime.now(tz=EST5EDT())
    processedDate = x.strftime("%m/%d/%Y, %I:%M:%S %p")
    table = dynamodb.Table(DYNAMODB_TABLE)
    item = {
        'id':data['id'],
        'Requestor':data['Requestor'],
        'RequestorEmail': data['RequestorEmail'],  # required
        'RequestCount': data['RequestCount'],
        'Contacts': data['Contacts'] if data['Contacts'] else None,
        'Persons': data['Persons'],
        'Birthdays': data['Birthdays'],
        'ZodiacSigns': data['ZodiacSigns'],
        'RequestDate': data['RequestDate'],
        'RequestStatus': requestStatus,
        'FlyerStatus': flyerStatus,
        'ProcessedDate':processedDate,
        'FlyerURLs':preSignedURLs
    }
    table.put_item(Item=item)
    return

def send_update_email(data):
    separator = '<br/>'
    contentLine1 = 'Request '+data['id']+' Updated.'+separator
    contentLine2 = 'Requestor - '+data['Requestor']+separator;
    contentLine3 = 'Requestor Email - '+data['RequestorEmail']+separator;
    contentLine4 = 'Contacts - '+data['Contacts']+separator;
    contentLine5 = 'Persons - '+data['Persons']+separator;
    contentLine6 = 'Birthdays - '+data['Birthdays']+separator;
    contentLine7 = 'Zodiac Signs - '+data['ZodiacSigns']+separator;
    contentLine8 = 'Request Date - '+data['RequestDate']+separator;
    contentLine9 = "Use Request Link to open - <a href=\""+S3_WEBSITE_URL+"/myrequests.html\">"+data['id']+"</a>"+separator
    
    content = contentLine1+contentLine2+contentLine3+contentLine4+contentLine5+contentLine6+contentLine7+contentLine8+contentLine9
    print(content)
    response = send_mail_to_user(data, content,data['RequestorEmail'])
    print("Sent email to requestor")
    contacts = data['Contacts']
    print("Contacts = '"+contacts+"'")
    if(len(contacts)>0):
         send_mail_to_user(data, content,contacts)
         print("Sent email to contacts")
         
def send_mail_to_user(data, content,emailIds):
    return ses.send_email(
        Source=SENDER_EMAIL,
        Destination={
            'ToAddresses': [
                emailIds,
            ]
        },
        Message={
            'Subject': {
                'Charset': CHARSET,
                'Data': 'ZodiacFlyer - Request Updated - ' + data['id']
            },
            'Body': {
                'Html': {
                    'Charset': CHARSET,
                    'Data': content
                },
                'Text': {
                    'Charset': CHARSET,
                    'Data': content
                }
            }
        }
    )


#upload_thumbnails("rrm987654321","Teddy Roosevelt,ohn Kennedy","27-Oct,29-May","Libra,Taurus")
def lambda_handler(event, context):
    #upload_thumbnails("mrm123456789","Teddy Roosevelt","27-Oct",sign)
    print(event)
    #print("Id - "+event['id']+" - "+event['Persons']+" "+event['Birthdays']+" "+event['ZodiacSigns'])
    update_to_dynamodb(event,"In Process","In Process","")
    send_update_email(event)
    preSignedURLs = upload_thumbnails(event['id'],event['Persons'],event['Birthdays'],event['ZodiacSigns'])
    update_to_dynamodb(event,"Processed","Processed",preSignedURLs)
    
    
    
    
    
    
    