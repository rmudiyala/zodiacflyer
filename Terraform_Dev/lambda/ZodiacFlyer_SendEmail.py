import os
import boto3


SENDER_EMAIL = os.environ['SENDER_EMAIL']
SES_REGION = os.environ['SES_REGION']
S3_WEBSITE_URL = os.environ['S3_WEBSITE_URL']
CHARSET = 'UTF-8'

ses = boto3.client('ses')

def lambda_handler(event, context):
    print(context)
    print(event)
    data = event
    
    separator = '<br/>'
    contentLine1 = 'Request '+data['id']+' processed.'+separator
    contentLine2 = 'Requestor - '+data['Requestor']+separator;
    contentLine3 = 'Requestor Email - '+data['RequestorEmail']+separator;
    contentLine4 = 'Contacts - '+data['Contacts']+separator;
    contentLine5 = 'Persons - '+data['Persons']+separator;
    contentLine6 = 'Birthdays - '+data['Birthdays']+separator;
    contentLine7 = 'Zodiac Signs - '+data['ZodiacSigns']+separator;
    contentLine8 = 'Request Date - '+data['RequestDate']+separator;
    contentLine9 = "Use Request Link to open - <a href=\""+S3_WEBSITE_URL+"/myrequests.html\">"+data['id']+"</a>"+separator
    
    content = contentLine1+contentLine2+contentLine3+contentLine4+contentLine5+contentLine6+contentLine7+contentLine8+contentLine9
    print(content)
    response = send_mail_to_user(data, content,data['RequestorEmail'])
    print("Sent email to requestor")
    contacts = data['Contacts']
    print("Contacts = '"+contacts+"'")
    if(len(contacts)>0):
         send_mail_to_user(data, content,contacts)
         print("Sent email to contacts")
    return 'Success!'

def send_mail_to_user(data, content,emailIds):
    return ses.send_email(
        Source=SENDER_EMAIL,
        Destination={
            'ToAddresses': [
                emailIds,
            ]
        },
        Message={
            'Subject': {
                'Charset': CHARSET,
                'Data': 'ZodiacFlyer - Request Processed - ' + data['id']
            },
            'Body': {
                'Html': {
                    'Charset': CHARSET,
                    'Data': content
                },
                'Text': {
                    'Charset': CHARSET,
                    'Data': content
                }
            }
        }
    )