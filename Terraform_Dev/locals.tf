locals {
  json_data = file("./data/zodiacflyer_requests.json")
  tf_data   = jsondecode(local.json_data)
}