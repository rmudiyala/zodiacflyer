resource "aws_iam_role" "iam_for_lambda" {
  name = "${var.application}_Lambda_Role_${var.environment}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

############################################################ ZodiacFlyer - GenerateFlyer Lambda Function #####################################################
data "archive_file" "Lambda_archive1" {
  type = "zip"

  source_file = "./lambda/${var.application}_${var.function1}.py"
  output_path = "./lambda/${var.application}_${var.function1}.zip"
}

resource "aws_s3_object" "Lambda_object1" {
  bucket = "${var.backendbucket}-${var.environment}"

  key    = "Lambda/${var.application}_${var.function1}.zip"
  source = data.archive_file.Lambda_archive1.output_path

  etag = filemd5(data.archive_file.Lambda_archive1.output_path)
}

resource "aws_lambda_function" "Lambda_Function1" {
  function_name = "${var.application}-${var.function1}-${var.environment}"

  s3_bucket = "${var.backendbucket}-${var.environment}"
  s3_key    = aws_s3_object.Lambda_object1.key

  runtime = "python3.8"
  handler = "${var.application}_GenerateFlyer.lambda_handler"

  source_code_hash = data.archive_file.Lambda_archive1.output_base64sha256
  role             = aws_iam_role.Lambda_Function1_Role.arn

  environment {
    variables = {
      DEST_BUCKET    = aws_s3_bucket.output-bucket.bucket
      DYNAMODB_TABLE = var.dynamodbtable
      S3_WEBSITE_URL = "https://${aws_s3_bucket.website-bucket.id}.s3.amazonaws.com"
      SENDER_EMAIL   = var.sender_email
      SES_REGION     = var.region
      SOURCE_BUCKET  = aws_s3_bucket.input-bucket.bucket
    }
  }

  tracing_config {
    mode = "Active"
  }
}

resource "aws_cloudwatch_log_group" "Lambda_Function1_LogGroup" {
  name              = "/aws/lambda/${aws_lambda_function.Lambda_Function1.function_name}"
  retention_in_days = 7
  #kms_key_id = aws_kms_key.apigw_lambda_key.arn
}

resource "aws_iam_role" "Lambda_Function1_Role" {
  name = "${var.application}-${var.environment}-Lambda-Function1-Role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy1" {
  role       = aws_iam_role.Lambda_Function1_Role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}


############################################################ ZodiacFlyer - StoreRequest Lambda Function #####################################################
data "archive_file" "Lambda_archive2" {
  type = "zip"

  source_file = "./lambda/${var.application}_${var.function2}.py"
  output_path = "./lambda/${var.application}_${var.function2}.zip"
}

resource "aws_s3_object" "Lambda_object2" {
  bucket = "${var.backendbucket}-${var.environment}"

  key    = "Lambda/${var.application}_${var.function2}.zip"
  source = data.archive_file.Lambda_archive2.output_path

  etag = filemd5(data.archive_file.Lambda_archive2.output_path)
}

resource "aws_lambda_function" "Lambda_Function2" {
  function_name = "${var.application}-${var.function2}-${var.environment}"

  s3_bucket = "${var.backendbucket}-${var.environment}"
  s3_key    = aws_s3_object.Lambda_object2.key

  runtime = "python3.9"
  handler = "${var.application}_${var.function2}.lambda_handler"

  source_code_hash = data.archive_file.Lambda_archive2.output_base64sha256
  role             = aws_iam_role.Lambda_Function2_Role.arn

  environment {
    variables = {
      DYNAMODB_TABLE   = var.dynamodbtable
      S3_WEBSITE_URL   = "https://${aws_s3_bucket.website-bucket.id}.s3.amazonaws.com"
      SENDER_EMAIL     = var.sender_email
      SES_REGION       = var.region
      STEPFUNCTION_ARN = "arn:aws:states:${var.region}:${var.account_id}:stateMachine:${var.application}-StepFunction-${var.environment}"
    }
  }

  tracing_config {
    mode = "Active"
  }
}

resource "aws_cloudwatch_log_group" "Lambda_Function2_LogGroup" {
  name              = "/aws/lambda/${aws_lambda_function.Lambda_Function2.function_name}"
  retention_in_days = 7
  #kms_key_id = aws_kms_key.apigw_lambda_key.arn
}

resource "aws_iam_role" "Lambda_Function2_Role" {
  name = "${var.application}-${var.environment}-Lambda-Function2-Role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy2" {
  role       = aws_iam_role.Lambda_Function2_Role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

############################################################ ZodiacFlyer - SendEmail Lambda Function #####################################################
data "archive_file" "Lambda_archive3" {
  type = "zip"

  source_file = "./lambda/${var.application}_${var.function3}.py"
  output_path = "./lambda/${var.application}_${var.function3}.zip"
}

resource "aws_s3_object" "Lambda_object3" {
  bucket = "${var.backendbucket}-${var.environment}"

  key    = "Lambda/${var.application}_${var.function3}.zip"
  source = data.archive_file.Lambda_archive3.output_path

  etag = filemd5(data.archive_file.Lambda_archive3.output_path)
}

resource "aws_lambda_function" "Lambda_Function3" {
  function_name = "${var.application}-${var.function3}-${var.environment}"

  s3_bucket = "${var.backendbucket}-${var.environment}"
  s3_key    = aws_s3_object.Lambda_object3.key

  runtime = "python3.9"
  handler = "${var.application}_${var.function3}.lambda_handler"

  source_code_hash = data.archive_file.Lambda_archive3.output_base64sha256
  role             = aws_iam_role.Lambda_Function3_Role.arn

  environment {
    variables = {
      S3_WEBSITE_URL = "https://${aws_s3_bucket.website-bucket.id}.s3.amazonaws.com"
      SENDER_EMAIL   = var.sender_email
      SES_REGION     = var.region
    }
  }

  tracing_config {
    mode = "Active"
  }

}

resource "aws_cloudwatch_log_group" "Lambda_Function3_LogGroup" {
  name              = "/aws/lambda/${aws_lambda_function.Lambda_Function3.function_name}"
  retention_in_days = 7
  #kms_key_id = aws_kms_key.apigw_lambda_key.arn
}

resource "aws_iam_role" "Lambda_Function3_Role" {
  name = "${var.application}-${var.environment}-Lambda-Function3-Role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy3" {
  role       = aws_iam_role.Lambda_Function3_Role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

############################################################ ZodiacFlyer - PerformCRUD Lambda Function #####################################################
data "archive_file" "Lambda_archive4" {
  type = "zip"

  source_file = "./lambda/${var.application}_${var.function4}.js"
  output_path = "./lambda/${var.application}_${var.function4}.zip"
}

resource "aws_s3_object" "Lambda_object4" {
  bucket = "${var.backendbucket}-${var.environment}"

  key    = "Lambda/${var.application}_${var.function4}.zip"
  source = data.archive_file.Lambda_archive4.output_path

  etag = filemd5(data.archive_file.Lambda_archive4.output_path)
}

resource "aws_lambda_function" "Lambda_Function4" {
  function_name = "${var.application}-${var.function4}-${var.environment}"

  s3_bucket = "${var.backendbucket}-${var.environment}"
  s3_key    = aws_s3_object.Lambda_object4.key

  runtime = "nodejs12.x"
  handler = "${var.application}_${var.function4}.lambda_handler"

  source_code_hash = data.archive_file.Lambda_archive4.output_base64sha256
  role             = aws_iam_role.Lambda_Function4_Role.arn

  environment {
    variables = {
      DYNAMODB_TABLE = var.dynamodbtable
      SES_REGION     = var.region
    }
  }

  tracing_config {
    mode = "Active"
  }
}

resource "aws_cloudwatch_log_group" "Lambda_Function4_LogGroup" {
  name              = "/aws/lambda/${aws_lambda_function.Lambda_Function4.function_name}"
  retention_in_days = 7
  #kms_key_id = aws_kms_key.apigw_lambda_key.arn
}

resource "aws_iam_role" "Lambda_Function4_Role" {
  name = "${var.application}-${var.environment}-Lambda-Function4-Role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy4" {
  role       = aws_iam_role.Lambda_Function4_Role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}