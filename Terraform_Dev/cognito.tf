resource "aws_cognito_user_pool" "pool" {
  name = "${var.application}-${var.environment}"



  schema {
    attribute_data_type = "String"
    mutable             = true
    name                = "email"
    required            = true
    string_attribute_constraints {
      min_length = 1
      max_length = 128
    }
  }



}

resource "aws_cognito_user_pool_client" "userpool_client" {
  name                                 = "${var.application}AppClient-${var.environment}"
  user_pool_id                         = aws_cognito_user_pool.pool.id
  callback_urls                        = ["https://${aws_s3_bucket.website-bucket.id}.s3.amazonaws.com/newrequest.html"]
  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_flows                  = ["code", "implicit"]
  allowed_oauth_scopes                 = ["email", "openid", "profile"]
  supported_identity_providers         = ["COGNITO"]

}

resource "aws_cognito_user_pool_domain" "userpool_domain" {
  domain       = var.cognitodomain
  user_pool_id = aws_cognito_user_pool.pool.id
}