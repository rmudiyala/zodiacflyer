###########################################################################################################################################
#    Project - AWS Cloud Challenge 4 - Zodiac Flyer                                                                                       #
#    Team    - Team 3                                                                                                                     #
#    Terraform - Create bucket and upload website                                                                                         #
#    Version - v1.0 (10-26-2022)                                                                                                          #
###########################################################################################################################################

#tfsec:ignore:aws-s3-enable-bucket-encryption tfsec:ignore:aws-s3-enable-bucket-logging

resource "aws_kms_key" "mykey" {
  description = "ZodiacFlyer app key used to encrypt bucket objects"
  #deletion_window_in_days = 10
}

resource "aws_s3_bucket" "log-bucket" {
  bucket = var.logbucket
  /*
  tags = {
    Account     = var.account
    Environment = var.environment
    Team        = var.team
    Application = var.application
  }
*/

  provisioner "local-exec" {
    when    = destroy
    command = "aws s3 rm s3://${self.bucket} --recursive"
  }
}

resource "aws_s3_bucket_versioning" "log-bucket" {
  bucket = aws_s3_bucket.log-bucket.bucket

  versioning_configuration {
    status = "Disabled"
  }
}

resource "aws_s3_bucket_public_access_block" "log-bucket" {
  bucket = aws_s3_bucket.log-bucket.bucket

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket" "input-bucket" {
  bucket = var.inputbucket

  /*
  tags = {
    Account     = var.account
    Environment = var.environment
    Team        = var.team
    Application = var.application
  }
*/

  provisioner "local-exec" {
    when    = destroy
    command = "aws s3 rm s3://${self.bucket} --recursive"
  }

  provisioner "local-exec" {
    when    = destroy
    command = "aws s3 rb s3://${self.bucket}"
  }

  provisioner "local-exec" {
    command = "aws s3 sync ./../Input s3://${self.bucket}"
  }
}

resource "aws_s3_bucket_logging" "input-bucket" {
  bucket        = aws_s3_bucket.input-bucket.bucket
  target_bucket = aws_s3_bucket.log-bucket.bucket
  target_prefix = "log/"
}

resource "aws_s3_bucket_versioning" "input-bucket" {
  bucket = aws_s3_bucket.input-bucket.bucket

  versioning_configuration {
    status = "Disabled"
  }
}

resource "aws_s3_bucket_public_access_block" "input-bucket" {
  bucket                  = aws_s3_bucket.input-bucket.bucket
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_server_side_encryption_configuration" "input-bucket" {
  bucket = aws_s3_bucket.input-bucket.bucket

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.mykey.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket" "website-bucket" {
  bucket        = var.websitebucket
  force_destroy = true

  /*
  tags = {
    Account     = var.account
    Environment = var.environment
    Team        = var.team
    Application = var.application
  }
*/

  provisioner "local-exec" {
    command = "aws s3 sync ./../website s3://${self.bucket}"
  }

  provisioner "local-exec" {
    when    = destroy
    command = "aws s3 rm s3://${self.bucket} --recursive"
  }
}

resource "aws_s3_bucket_versioning" "website-bucket" {
  bucket = aws_s3_bucket.website-bucket.bucket

  versioning_configuration {
    status = "Disabled"
  }
}

resource "aws_s3_bucket_logging" "website-bucket" {
  bucket        = aws_s3_bucket.website-bucket.bucket
  target_bucket = aws_s3_bucket.log-bucket.bucket
  target_prefix = "log/"
}

resource "aws_s3_bucket_public_access_block" "website-bucket" {
  bucket                  = aws_s3_bucket.website-bucket.bucket
  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

/*
resource "aws_s3_bucket_server_side_encryption_configuration" "website-bucket" {
  bucket = aws_s3_bucket.website-bucket.bucket

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.mykey.arn
      sse_algorithm     = "aws:kms"
    }
  }
}
*/

resource "aws_s3_bucket_website_configuration" "website-bucket" {
  bucket = aws_s3_bucket.website-bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

resource "aws_s3_bucket_policy" "website-bucket" {
  bucket = aws_s3_bucket.website-bucket.bucket

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "Policy1645724938586",
    "Statement": [
        {
            "Sid": "Stmt1645724933619",
            "Effect": "Allow",
            "Principal": "*",
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.website-bucket.bucket}/*"
        }
    ]
}
POLICY
}

/*
resource "null_resource" "website-opener" {
    provisioner "local-exec" {
    command = "msedge.exe http://${aws_s3_bucket.website-bucket.bucket}.s3-website.us-east-1.amazonaws.com"
  }
}
*/